import React from "react";
import Header from "../../components/header/header";
import EmptyContent from "../../components/empty/emptyContent";
import Hero from "../../components/hero/hero";
import Contenu from "../../components/contenu/contenu";
import ServiceCard from "../../components/service-card/service-card";
import ServiceCardSecond from "../../components/service-card-second/service-card-second";
import Footer from "../../components/footer/footer";


const Dashboard = () => {
    return (
        <div >
            <Header />
            <EmptyContent />
            <Hero/>  
            <Contenu />
            <ServiceCard />
            <ServiceCardSecond />
            <Footer/>
        </div>
    );
}

export default Dashboard;