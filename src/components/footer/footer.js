import React from 'react';
import './footer.css';
import { Input, Space } from 'antd';
const { Search } = Input;

const Footer = () => {

  const onSearch = (value, _e, info) => console.log(info?.source, value);

  return (
    <div className="proprio-footer">
      <div className="proprio-footer-content">
        <div className="proprio-footer-content-item01">
          <h1>Join Our Newsletter Now</h1>
          <h5>Register now to get updates on promotions...</h5>
        </div>

        <div className="proprio-footer-content-item02">
          
          <Search
            activeBorderColor='#0D263B'
            placeholder="Enter your email To Subscribe..."
            allowClear
            enterButton="SUBSCRIBE"
            size="large"
            onSearch={onSearch}
          /> 
          

        </div>

      </div>
    </div>
  );
}

export default Footer;
