import React from 'react';
import './service-card.css';


const ServiceCard = () => {
  return (
    <div className="proprio-service-card">
          <div className="proprio-service-card-content">
            <div className="proprio-service-card-content-title">
                <h1>CTA</h1>
            </div><br />
            <h6>lorem ipsum</h6><br />
            <div className="proprio-service-card-content-button">CTA</div>
          </div>
    </div>
  );
}

export default ServiceCard;
