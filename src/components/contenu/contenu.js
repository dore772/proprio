import React , { useState } from 'react';
import "./contenu.css";
import { Tabs } from 'antd';
import { UserOutlined, EllipsisOutlined, PlusOutlined, ArrowRightOutlined, EditOutlined, EyeOutlined } from '@ant-design/icons';
import { Table, Space } from "antd";

const { TabPane } = Tabs;

const avatar = require('../../assets/avatarImage.png');
const cardOne = require('../../assets/card.png');


const Contenu = () => {

    const [selectedRowKeys, setSelectedRowKeys] = useState([]);

    const columns = [
        {
            title: 'Property Name',
            dataIndex: 'propertyName',
            key: 'propertyName',
        },
        {
            title: 'Type',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
        },
        {
            title: 'Beds',
            dataIndex: 'beds',
            key: 'beds',
        },
        {
            title: 'Bath',
            dataIndex: 'bath',
            key: 'bath',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Author',
            dataIndex: 'author',
            key: 'author',
            render: (_, record) => (
            <Space size="middle">
                <EditOutlined />
                <EyeOutlined />
            </Space>
    ),
        },
    ];

    const dataSource = [
        {
            key: '1',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },

        {
            key: '2',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },

        {
            key: '3',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },

        {
            key: '4',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },

        {
            key: '5',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },

        
        {
            key: '6',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '7',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '8',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '9',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '10',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '11',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '12',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '13',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '14',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '15',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '16',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '17',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '18',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '19',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        {
            key: '20',
            propertyName: 'Ocean Villa',
            type: 'House',
            location: 'Abidjan',
            price: '$350,000',
            beds: '4',
            bath: '3',
            status: 'Active',
            author: ''

        },
        
    ];

    const onSelectChange = (newSelectedRowKeys) => {
        console.log('selectedRowKeys changed: ', newSelectedRowKeys);
        setSelectedRowKeys(newSelectedRowKeys);
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
   
    
    return (
        <div className='proprio-content'>

            <div className='proprio-content-left-side'>
                <div className='proprio-content-left-side-card'>
                    <div className='proprio-content-left-side-card-item'>
                        <h1>25%</h1>
                        <p>Lead Conversion Rate</p>
                    </div>
                    <div className='proprio-content-left-side-card-item'>
                        <h1>10%</h1>
                        <p>Listing Click-Through Rate (CTR)</p>
                    </div>

                    <div className='proprio-content-left-side-card-item'>
                        <h1>3 Min</h1>
                        <p>Average Time On Property Pages</p>
                    </div>
                </div>

                <div className='proprio-content-left-side-card-tabs'>
                    <Tabs defaultActiveKey="1" scrollable>
                        <TabPane tab="Active" key="1">
                        </TabPane>
                        <TabPane tab="Past" key="2">
                            {/* Contenu du tableau pour l'onglet 2 */}
                        </TabPane>
                        <TabPane tab="For sale" key="3">
                            {/* Contenu du tableau pour l'onglet 3 */}
                        </TabPane>
                        <TabPane tab="For rent" key="4">
                            {/* Contenu du tableau pour l'onglet 3 */}
                        </TabPane>
                        <TabPane tab="For commercial" key="5">
                            {/* Contenu du tableau pour l'onglet 3 */}
                        </TabPane>
                        <TabPane tab="Land" key="6">
                            {/* Contenu du tableau pour l'onglet 3 */}
                        </TabPane>
                    </Tabs>
                </div>

                <div className='proprio-content-left-side-card-table'>
                    <Table rowSelection={rowSelection} dataSource={dataSource} columns={columns} className='my-table'/>
                </div>

                
               
                {/* <Table rowSelection={rowSelection} dataSource={dataSource} columns={columns} className='my-table'/> */}

            </div>

            
            <div className='proprio-content-right-side'>

                <div className='proprio-content-right-side-head'>
                    <UserOutlined />
                    <EllipsisOutlined />
                </div>

                <div className='proprio-content-right-side-avatarContent'>
                    
                    <div className="avatar-contenu-container">
                        <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>
                    <div className="icon-overlay">
                        <div className="icon-rond">
                            <PlusOutlined />
                        </div>
                    </div>
                </div>

                <div className='title-avatar'>Hayes Real Estate</div>
                
                <div className='proprio-content-right-side-payement'>
                    <h6>Payment Details</h6>
                    <EllipsisOutlined />
                </div>

                <div className="card-with-arrow">
                    <img src={cardOne} alt="Card" className="card-image"/>
                    <div className="card-content">
                        <h6 className="card-title">Master Card(Default)</h6>
                        <h6 className="card-subtitle">Exp:2/2023</h6>
                    </div>
                    <div className="arrow-icon" >
                        <div className="arrow-icon-rounded">
                            <ArrowRightOutlined/>
                        </div>
                    </div>
                </div>
                
                <div className="card-with-arrow">
                    <img src={cardOne} alt="Card" className="card-image"/>
                    <div className="card-content">
                        <h6 className="card-title">Master Card(Default)</h6>
                        <h6 className="card-subtitle">Exp:2/2023</h6>
                    </div>
                    <div className="arrow-icon" >
                        <div className="arrow-icon-rounded">
                            <ArrowRightOutlined/>
                        </div>
                    </div>
                </div>
                
                <div className="card-with-arrow">
                    <img src={cardOne} alt="Card" className="card-image"/>
                    <div className="card-content">
                        <h6 className="card-title">Master Card(Default)</h6>
                        <h6 className="card-subtitle">Exp:2/2023</h6>
                    </div>
                    <div className="arrow-icon" >
                        <div className="arrow-icon-rounded">
                            <ArrowRightOutlined/>
                        </div>
                    </div>
                </div>

                <div className='proprio-content-right-side-payement'>
                    <h6>Your team(9)</h6>
                    <EllipsisOutlined />
                </div>

                
                <div className='proprio-content-right-side-avatar-group'>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>

                    <div className="avatar-rounded">
                            <img src={avatar} alt="proprio avatar" className="avatar-contenu-image" />
                    </div>
                    
                </div>


                
                
            </div>

        </div>  
    );
}

export default Contenu;