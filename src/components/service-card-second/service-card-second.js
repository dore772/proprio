import React from 'react';
import './service-card-second.css';

const title = require('../../assets/title.png');

const facebook = require('../../assets/Facebook.png');
const instagram = require('../../assets/Google.png');
const linkdin = require('../../assets/Linkedin.png');
const twitter = require('../../assets/Twitter.png');
const address = require('../../assets/Adress.png');
const phone = require('../../assets/Phone.png');
const email = require('../../assets/Email.png');

const ServiceCardSecond = () => {
  return (
      <div className="proprio-service-card-second">
      <div className="proprio-service-card-second-content">
        
        <div className="proprio-service-card-second-content-item">
          
              <div className="proprio-service-card-second-content-item-1">
                <img src={title} alt="proprio logo" className="logo" />
                <p>
                    Lorem Ipsum is simply dummy text of the and typesetting industry. Lorem Ipsum is dummy text of the printing.
                </p>
                <div className="proprio-service-card-second-content-item-1-social">
                    <img src={facebook} alt="proprio logo" className="logo-social" />
                    <img src={instagram} alt="proprio logo" className="logo-social" />
                    <img src={linkdin} alt="proprio logo" className="logo-social" />
                    <img src={twitter} alt="proprio logo" className="logo-social" />
                </div>
              </div>
          
               <div className="proprio-service-card-second-content-item-2">
                  <h2>Quick Links</h2>
                  <h5>Home</h5>
                  <h5>About Us</h5>
                  <h5>Blog</h5>
                  <h5>FAQ</h5>
                  <h5>Services</h5>
               </div>
           
              <div className="proprio-service-card-second-content-item-3">
                  <h2>Services</h2>
                  <h5>Wish List</h5>
                  <h5>Login</h5>
                  <h5>Submit a Request</h5>
                  <h5>Appointment</h5>
                  <h5>Promotional Offers</h5>
              </div>
              <div className="proprio-service-card-second-content-item-4">
                  <h2>Contract</h2>
                  <p>
                    <img src={address} alt="proprio logo" className="logo-socials" />
                    
                  </p>
                  <p>
                    <img src={phone} alt="proprio logo" className="logo-socials" />
                          
                  </p>
                  <p>
                      <img src={email} alt="proprio logo" className="logo-socials" />
                  </p>
              </div>
            </div>
        
            <div className="divider"></div>
        
            <div className="proprio-service-card-second-content-copyrigh">
                  <div className="copyrigh">
                      © Copyright Medih 2022 All Right Reserved.
                  </div>
                  <div className="copyrigh-info">
                      <span>Terms Of Use &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Privacy Policy</span>
                  </div>
            </div>
        
        </div>
    </div>
  );
}

export default ServiceCardSecond;
