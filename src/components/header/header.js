import React from "react";
import "./header.css";
import { QuestionCircleOutlined } from "@ant-design/icons";

const logo = require('../../assets/logo.png');
const avatar = require('../../assets/avatar.png');

const Header = () => {
    return (
        <div className="proprio-header">

            <div className="proprio-header-content">

                <div className="proprio-header-first-content">
                    <div className="proprio-header-content-items-logo">
                        <img src={logo} alt="proprio logo"/>
                     </div>
                    
                    <div className="proprio-header-content-items">
                        <div className="proprio-header-content-forsales"><a href="#">For sale</a></div>
                        <div className="proprio-header-content-forrent"><a a href="#">For Rent</a></div>
                        <div className="proprio-header-content-land"><a href="#">Land</a></div>
                        <div className="proprio-header-content-commercial"><a href="#">Commercial</a></div>
                        <div className="proprio-header-content-foragent"><a href="#">For Agent</a></div>
                    </div>
                </div>

                <div className="proprio-header-second-content">
                    <QuestionCircleOutlined color=""/>
                    <h5>Log off</h5>
                    <div className="avatar-container">
                        <img src={avatar} alt="proprio avatar" className="avatar-image" />
                     </div>
                    
                </div>
                

            </div>

        </div>
    );
}

export default Header;
