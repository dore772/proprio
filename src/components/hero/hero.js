import React from "react";
import "./hero.css";
import { PlusOutlined, AppstoreOutlined } from '@ant-design/icons';

const Hero = () => {
    return (
        <div className="proprio-hero">

            <div className="proprio-hero-content">

                <div className="proprio-hero-first-content">
                    <div className="proprio-hero-content-itemsOne">
                        
                        <AppstoreOutlined />
                        <h5>Dashbord</h5>
                    </div>
                    <div className="proprio-hero-content-itemsTwo">
                        
                        <AppstoreOutlined />
                        <h5>Account Details</h5>
                    </div>
                </div>

                <div className="proprio-hero-second-content">
                    <PlusOutlined />
                    <h5>New Listing</h5>
                </div>
                

            </div>

        </div>
    );
}

export default Hero;
