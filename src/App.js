
import './App.css';
import Dashboard from './pages/dashboard/dashboard';

function App() {
  return (
    <div className="appContent">
      <Dashboard/> 
    </div>
  );
}

export default App;
